import math
tape_thickness_mm = 0.1
roll_radius_mm = 40
full_roll_radius_mm = 55
pie = math.pi
roll_diameter_mm = 2 * roll_radius_mm * pie
current_roll_radius_mm = roll_radius_mm
tape_amount_mm = 0
while current_roll_radius_mm < full_roll_radius_mm:
    tape_amount_mm += roll_diameter_mm
    current_roll_radius_mm += tape_thickness_mm
    roll_diameter_mm = 2 * current_roll_radius_mm * pie
print(tape_amount_mm/1000)